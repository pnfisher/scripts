#!/bin/bash

pname=$(basename $0)

if [ $(whoami) != "root" ]; then
  echo "error, please run using sudo" 1>&2
  exit 1
fi

## http://odyniec.net/articles/ubuntu-subversion-server/
sudo apt-get install subversion

# You're going to need a directory for your repositories, as well as
# other Subversion-related files. Most people use /home/svn or
# /usr/local/svn for this purpose, and you can choose either. I
# personally prefer /usr/local/svn over /home/svn, as I like to keep
# /home for home directories of real users of the system.
mkdir -p /usr/local/svn

# Inside this directory, create another one to hold your repositories:
mkdir -p /usr/local/svn/repos

# Now, you need to set some access permissions on those
# directories. You only want to allow certain users of your system
# (that is, yourself and the other developers) to access the
# repositories, so add a new group for those users. Name the group
# svn.
groupadd svn

# Then, change the group ownership of /usr/local/svn/repos to the new
# group using the chgrp command:
chgrp svn /usr/local/svn/repos

# The members of the svn group also need write access to the repos
# directory, so use chmod to add the write permission for the group:
chmod g+w /usr/local/svn/repos

# Additionally, you need to make sure that all new files and
# directories created in the repos directory (in other words, anything
# committed to the repositories) will also be owned by the group. To
# accomplish this, use chmod again to set the set-group-ID bit on the
# directory, which causes any file created inside it to have the same
# group ownership as the directory itself. Effectively, everything in
# repos will belong to the svn group.
chmod g+s /usr/local/svn/repos

# OK, so you now have the repositories directory with proper
# permissions, ready to be used by the svn group. Go ahead and add
# yourself to the group:
usermod -a -G svn phil

# However, your new group membership will not be effective for the
# current session, so you need to log out and log back in. When you're
# back, you can verify that your account is recognized as a member of
# the svn group:

rm -f /usr/local/svn/passwd
cp passwd /usr/local/svn/
chmod 600 /usr/local/svn/passwd
