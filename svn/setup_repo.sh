#!/bin/bash

if [ -z "$1" ]; then
  echo "error, missing repo argument" 2>&1
  exit 1
fi

repo=$1

if [ -d /usr/local/svn/repos/${repo} ]; then
  while true; do
    read -p "Repo ${repo} already exists. Delete (y/n)? " yn
    case $yn in
      [Yy]*) break;;
      [Nn]*) exit 0;;
      *) echo "Please answer y or n.";;
    esac
  done
fi
rm -rf /usr/local/svn/repos/${repo}

# Creating a Test Repository. You can now create a repository. In the
# following steps, I'll demonstrate how to create a simple test
# repository containing one text file, and how to check out and commit
# files. If you're not familiar with Subversion, then this could be a
# good exercise to learn the basics. Otherwise, you can skip all the
# test checkouts and commits and just create the repository for your
# project.

# The repository will be a subdirectory in the repos directory, and
# will have its group ownership set to svn (thanks to the chmod g+s
# you did earlier). However, that's not all, you also need to make
# sure the repository will be group writable, so that the other
# members of the svn group will be able to commit files. To do this,
# set the umask to 002:
umask 002

# This command sets the new file mode creation mask which controls the
# default permissions of any new file that you create. The default
# value is 022 and it corresponds to read/write permissions for the
# file owner, and read permissions for the group and others. The new
# value, 002, also gives write permissions to the group, which is just
# what you need.

# Create the repository using the svnadmin command:
svnadmin create /usr/local/svn/repos/${repo}

# And set back the default umask:
umask 022

rm -f /usr/local/svn/repos/${repo}/conf/svnserve.conf
cp svnserve.conf /usr/local/svn/repos/${repo}/conf/
