#!/bin/bash

pname=$(basename $0)

if [ $(whoami) != "root" ]; then
  echo "error, please run using sudo" 1>&2
  exit 1
fi

svnserve -d --foreground -r /usr/local/svn/repos
