#!/bin/bash

pwd=`pwd`
scripts=`ls ${pwd}/*.sh`

mkdir -p ${HOME}/bin
chmod ugo+x ${HOME}/bin

for s in ${scripts}; do
  lnk=`basename ${s%.*}`
  if [ ${lnk} == "setup" ]; then
    continue
  fi
  ln -sf $s ~/bin/${lnk}
done

# specials
if [ -f /etc/cygport.conf ]; then
  # when working with visual studio, we need to put windows stuff at
  # the front of the path (before cygwin stuff), but then we pickup
  # windows version of various utils. Let's selectively restore some
  # of the linux utils back to the front of our path
  ln -sf /usr/bin/find ~/bin/.
  ln -sf /usr/bin/sort ~/bin/.
  ln -sf /usr/bin/hostname ~/bin/.
  ln -sf /usr/bin/whoami ~/bin/.
  ln -sf /usr/bin/ssh* ~/bin/.
fi
ln -sf ~/src/adbfs-rootless/adbfs ~/bin/.
