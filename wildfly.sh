#!/bin/bash

commands() {
  echo "supported commands are:" 1>&2
  echo "  -- special -----" 1>&2
  echo "  console" 1>&2
  echo "  debug" 1>&2
  echo "  deploy|load" 1>&2
  echo "  deployed|loaded" 1>&2
  echo "  full" 1>&2
  echo "  undeploy|unload" 1>&2
  echo "  quickstart|guide" 1>&2
  echo "  shutdown" 1>&2
  echo "  -- official ----" 1>&2
  cd ${wdir} || exit 1
  for f in *.sh; do
    [ ! -x $f ] && continue
    echo "  ${f%.sh}" 1>&2
  done
}

JDK=${JDK:-8}
wdir=${HOME}/java/wildfly/bin

if [ ! -d ${wdir} ]; then
  echo "error, directory '${wdir}' is missing" 1>&2
  exit 1
fi

if (($# == 0)); then
  echo "error, command argument is missing" 1>&2
  commands
  exit 1
fi

if [ "$JDK" != "7" -a "$JDK" != "8" ]; then
  echo "error, invalid JDK setting (${JDK})" 1>&2
  exit 1
fi

echo "$(basename $0 .sh): using jdk ${JDK}"

if [ "$JDK" == "8" ]; then
  export JAVA_HOME=${HOME}/java/jdk1.8.0_60
  export PATH=${JAVA_HOME}/bin:${PATH}
fi

cmd=$1
shift
if [ "$cmd" == "quickstart" -o "$cmd" == "guide" ]; then
  qdir=${HOME}/src/java/examples/wildfly/quickstart/guide/target/guides/html
  cd ${qdir} || {
    echo "error, directory '${qdir}' is missing" 1>&2;
    exit 1
  }
  /usr/bin/firefox file:///${qdir} &
  exit
fi

if [ "$cmd" == "deployed" -o "$cmd" == "loaded" ]; then
  ${wdir}/jboss-cli.sh --connect --command="deployment-info"
  exit $?
fi

if [ "$cmd" == "deploy" -o "$cmd" == "load" ]; then
  if (($# < 1)); then
    echo "error, deploy arguments missing" 1>&2
    exit 1
  fi
  if [[ $* =~ ([^ ]+)$ ]] && [ ! -f ${BASH_REMATCH[1]} ]; then
    echo "error, ${BASH_REMATCH[1]} missing" 1>&2
    exit 1
  fi
  ${wdir}/jboss-cli.sh --connect --command="deploy $*"
  exit $?
fi

if [ "$cmd" == "undeploy" -o "$cmd" == "unload" ]; then
  ${wdir}/jboss-cli.sh --connect --command="version" >& /dev/null
  if (($? != 0)); then
    echo "error, couldn't connect to wildfly server" 1>&2
    exit 1
  fi
  apps=; i=0;
  while read line; do
    if [[ $line =~ ^([^\.]*\.(war|jar|ear) ).* ]]; then
      apps[$i]=${BASH_REMATCH[1]}
      let i=i+1
    fi
  done < <(${wdir}/jboss-cli.sh --connect --command="deployment-info" | \
           tail -n +1)
  (($? != 0)) && exit $?
  if ((i == 0)); then
    echo "nothing to ${cmd}"
    exit 0
  fi
  for app in ${apps[@]}; do
    echo undeploying ${app}
    ${wdir}/jboss-cli.sh --connect --command="undeploy ${app}"
    (($? != 0)) && echo "error, undeploying ${app}" 1>&2
  done
  exit $?
fi

if [ "$cmd" == "console" ]; then
  /usr/bin/firefox http://localhost:9990 &
  exit 0
fi

if [ "$cmd" == "debug" ]; then
  ${wdir}/standalone.sh --debug -b 0.0.0.0 -bmanagement 0.0.0.0 $*
  exit $?
fi

if [ "$cmd" == "full" ]; then
  ${wdir}/standalone.sh -b 0.0.0.0 -bmanagement 0.0.0.0 \
         -c standalone-full.xml $*
  exit $?
fi

if [ "$cmd" == "shutdown" ]; then
  ${wdir}/jboss-cli.sh --connect command=:shutdown
  exit $?
fi

if [ ! -x ${wdir}/${cmd}.sh ]; then
  echo "error, '${cmd}' is not a supported command"
  commands
  exit 1
fi

${wdir}/${cmd}.sh $*
