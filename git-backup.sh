#!/bin/bash

p=$(basename $0)
dir=$(basename $(pwd))
repo=/home/phil/backup/repos/${dir}
rsyncignore=".rsyncignore"

if [ ! -d .git ]; then
  echo $p: current directory does not appear to be a git repo
  exit 1
fi

if [ ! -f ${rsyncignore} ]; then
  echo $p: current directory does not have a ${rsyncignore} file
  exit 1
fi

[ ! -d ${repo} ] && mkdir -p ${repo}

set -x
git gc
rsync -a --info=progress2 --delete --exclude-from ${rsyncignore} \
../${dir} ${repo}/..
