#!/bin/bash

p=$(basename $0)

(($# < 1)) && {
  echo "$p: error, missing target host argument" 1>&2;
  echo "usage: myemacs host [socket]" 1>&2;
  exit 1;
}

os=$(uname -o)
host=$(hostname)

if [ "$1" == "@@" ]; then
  thost=${host}
  socket=${host}
else
  thost=$1
  [ "$thost" == "@" ] && thost=${host}
  socket=$2
  [ "$socket" == "@" ] && socket=${thost}
fi

for h in arm firma dev07 dev08; do
  if [ "$thost" = "$h" ]; then
    thost+=".dev.ecobee.com"
    break;
  fi
done  

[ -z "$USER" ] && [ ! -z "$USERNAME" ] && USER=${USERNAME}
if [ "$thost" = "tp" ] || [ "$thost" = "TP" ]; then
  thost=TP-20141493
fi

if [[ $thost =~ ([^@]*)@(.*)$ ]]; then
  USER=${BASH_REMATCH[1]}
  phost=${BASH_REMATCH[2]}
else
  phost=${thost}
fi

#[ -z "$TITLE" ] && TITLE="${USER}@${phost}"
TITLE="${USER}@${phost}"
[ ! -z "${SUBTITLE}" ] && TITLE="$TITLE/[$SUBTITLE]"

SETENV=". ${HOME}/.profile; . ${HOME}/.bashrc"
[ "$thost" = "TP-20141493" ] && SETENV="$SETENV; export USER=${USER}"
#DCMD="emacs --daemon=${socket} >& /dev/null"
DCMD="emacs --daemon=${socket}"
ECMD="emacs -T ${TITLE} < /dev/null >& /dev/null"
## use of after-make-frame-functions hook in my-xwin.el
## makes the explicit call to my-xwin-init below no longer
## necessary
# ECCMD="
# emacsclient \
# --socket-name=${socket} \
# --frame-parameters='(\
# (name . \"emacsclient@${thost}\") \
# (background-color . \"black\") \
# (foreground-color . \"gray80\"))' \
# -c \
# --eval '(my-xwin-init)' < /dev/null >& /dev/null"
ECCMD="
emacsclient \
--socket-name=${socket} \
--frame-parameters='(\
(name . \"emacsclient@${phost}\") \
(background-color . \"black\") \
(foreground-color . \"gray80\"))' \
-c < /dev/null >& /dev/null"

startup() {

  if [ "$1" != "local" ] && [ "$1" != "remote" ]; then
    echo $p: error in startup, unknown arg $1 1>&2
    exit 1
  fi

  echo "$p: starting server on ${thost} (socket = ${socket})"
  if [ "$1" = "local" ]; then
    eval ${DCMD}
  else
    ssh -n ${thost} "${SETENV}; ${DCMD}"
  fi
  [ $? != 0 ] && \
    echo "$p: ignoring server startup error (assuming server already running)"

  echo "$p: starting client on ${thost}, (socket = ${socket})"
  if [ "$1" = "local" ]; then
    eval ${ECCMD} &
  else
    ssh -n ${thost} "${SETENV}; ${ECCMD}" &
  fi

}

if [ "$thost" != "$host" ]; then

  # special case for target host using ssh port forwarding
  if [ "$host" = "TP-20141493" ] && [ "$thost" = "brome" ]; then
    ssh -o ConnectTimeout=2 -o ConnectionAttempts=1 brome uptime >& /dev/null
    if [ $? != 0 ]; then
      echo "$p: can't ssh into target host 'brome'" 1>&2
      exit 1
    fi
  else
    count_arg="-c"
    [ "$os" = "Cygwin" ] && count_arg="-n"
    ping ${count_arg} 1 ${phost} &> /dev/null
    if [ $? != 0 ]; then
      echo $p: target host \'${phost}\' not responding to ping 1>&2
      exit 1
    fi
    sshkeygen=$(ssh ${thost} uptime 2>&1 > /dev/null | grep ssh-keygen)
    if (($? == 0)); then
      echo "warning, man-in-the-middle problems with ${thost}"
      echo running ssh-keygen -f ${HOME}/.ssh/known_hosts -R ${thost}
      ssh-keygen -f ${HOME}/.ssh/known_hosts -R ${thost}
      if (($? != 0)); then
        echo -n "error, ssh-keygen -f " 1>&2
        echo "${HOME}/.ssh/known_hosts -R ${thost} failed" 1>&2
        exit 1
      fi
    fi
  fi

  ssh ${thost} ". ${HOME}/.profile && which emacs >& /dev/null"
  if (($? != 0)); then
    echo "error, no emacs in path on ${thost}" 1>&2
    exit 1
  fi

  if [ -n "$socket" ]; then
    startup remote
  else
    ssh -n ${thost} -R 19216:localhost:22 "bash -l -c \"${SETENV}; X11CLIENT=${host} ${ECMD}\"" &
    #ssh -n ${thost} "${SETENV}; X11CLIENT=${host} ${ECMD}" &
  fi

  exit

fi

which emacs >& /dev/null
if (($? != 0)); then
  echo "error, no emacs in path" 1>&2
  exit 1
fi

if [ -n "$socket" ]; then
  startup local
else
  eval ${ECMD} &
fi
