#!/bin/sh

switch=br0

if [ -n "$1" ];then
  sudo /sbin/brctl delif $switch $1
  sudo /sbin/ifconfig $1 0.0.0.0 down
  #/sbin/ip link set $1 down
  sudo /usr/sbin/tunctl -d $1
  exit 0
else
  echo "Error: no interface specified"
  exit 1
fi
