#!/bin/sh -x

usage () {

  echo $p: usage
  echo "-pb  # backup on Philbook"
  echo "-sea # backup on Seagate"
  echo "-v   # verbose rsync"
  echo 

  exit 1

}

p=`basename $0`
host=`hostname`
seagate=
philbook=
verbose=

while [ $# -gt 0 ]; do
  case $1 in
   -pb) philbook=1;;
   -sea) seagate=1;;
   -v) verbose="--progress";;
   *) usage;;
  esac
  shift
done

if [ -z "${seagate}" ] && [ -z "${philbook}" ]; then
  echo $p: error, you must specify one of -pb or -sea
  exit 1
fi

if [ ! -z "${seagate}" ] && [ ! -z "${philbook}" ]; then
  echo $p: error, you must specify one of -pb or -sea, but not both
  exit 1
fi

[ ! -z "${philbook}" ] && \
echo $p: error, philbook currently not supported && \
exit 1

target_dir=

if [ ! -z "${seagate}" ]; then
  target_dir="/media/Seagate Backup Plus Drive/Backups/home.${host}"
else
  sudo mkdir -p /mnt/PhilBook
  sudo mount -t cifs -o user=phil,rw,nodev,nosuid,sfu,uid=phil \
  //PhilBook/phil /mnt/PhilBook
  target_dir="/mnt/PhilBook/Backups/home.${host}"
fi

[ ! -d "${target_dir}" ] && \
echo $p: "${target_dir}"  missing && \
exit 1

echo $p: please be patient...
set -x
time rsync -ahO --delete \
--exclude 'media' \
--exclude 'Downloads' \
--exclude 'download' \
--exclude '.Philbook' \
${verbose} /home/phil/ "${target_dir}"

if [ ! -z "${philbook}" ]; then
  sudo umount /mnt/PhilBook
fi
