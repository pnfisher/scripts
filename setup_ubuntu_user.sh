#!/bin/bash

pname=$(basename $0)

if [ $(whoami) == "root" ]; then
  echo "error, please don't run this script using sudo" 1>&2
  exit 1
fi

. /etc/lsb-release || {
  echo "error, couldn't source /etc/lsb-release" 1>&2;
  exit 1;
}

if [ "$DISTRIB_CODENAME" != "xenial" ]; then
  echo "error, current distro \'${DISTRIB_CODENAME}\' not supported" 1>&2
  exit 1
fi

# you have to clone scripts manually (and do ./setup.sh as well) in
# order to get this script
if [ ! -f ~/src/scripts/setup_user.sh ]; then
  echo "error, you haven't cloned ~/src/scripts repo" 1>&2
  exit 1
fi

# ssh keys
if [ ! -f ~/.ssh/id_rsa -o ! -f ~/.ssh/id_rsa.pub ]; then
  mkdir -p ~/.ssh
  rm -f ~/.ssh/id_rsa*
  (cd ~/.ssh && cat /dev/zero | ssh-keygen -q -N "" >& /dev/null)
fi

# have we setup bitbucket so we can clone my repos using ssh?
while true; do
  read -p "have you setup a bitbucket ssh key for this machine? " yn
  case $yn in
    [Yy]*) break;;
    [Nn]*) exit;;
    *) echo "Please answer y or n.";;
  esac
done

# install basic packages for this distro using sudo?
setup_host=~/src/scripts/setup_ubuntu_host.sh
if [ -f ${setup_host} ]; then
  while true; do
    read -p "run ${setup_host} using sudo? " yn
    case $yn in
      [Yy]*) sudo ${setup_host}; break;;
      [Nn]*) break;;
      *) echo "Please answer y or n.";;
    esac
  done
fi

# do we need to run ssh-copy-id to the current host (so we can use
# ansible when provisioning ulxc systems)
timeout 4s ssh $(hostname) exit >& /dev/null
if [ $? != 0 ]; then
  echo "couldn't ssh to $(hostname) without password, running ssh-copy-id" 1>&2
  ssh-copy-id -i ~/.ssh/id_rsa $(hostname)
  if [ $? != 0 ]; then
    echo "error, ssh-copy-id -i ~/.ssh/id_rsa $(hostname) failed" 1>&2
    exit 1
  fi
fi

# gnome-session-flashback tweaks
gsettings set org.gnome.metacity theme Radiance
gsettings set org.gnome.desktop.wm.preferences theme Radiance
gsettings set org.gnome.desktop.interface gtk-theme Radiance
gsettings set org.gnome.gnome-panel.toplevel:/org/gnome/gnome-panel/layout/toplevels/top-panel/ auto-hide true
gsettings set org.gnome.gnome-panel.toplevel:/org/gnome/gnome-panel/layout/toplevels/top-panel/ enable-arrows false
gsettings set org.gnome.gnome-panel.toplevel:/org/gnome/gnome-panel/layout/toplevels/bottom-panel/ auto-hide true
gsettings set org.gnome.gnome-panel.toplevel:/org/gnome/gnome-panel/layout/toplevels/bottom-panel/ enable-arrows false

# autostart myxplanet.sh 
if [ ! -f ~/.config/autostart/myxplanet.desktop ]; then
mkdir -p ~/.config/autostart
cat <<EOF > ~/.config/autostart/myxplanet.desktop
[Desktop Entry]
Type=Application
Exec=/home/${USER}/bin/myxplanet
Hidden=false
NoDisplay=false
X-GNOME-Autostart-enabled=true
Name[en_CA]=xplanet
Name=xplanet
Comment[en_CA]=
Comment=
EOF
fi

# autostart yakuake
if [ ! -f ~/.config/autostart/yakuake.desktop ]; then
mkdir -p ~/.config/autostart
cat <<EOF > ~/.config/autostart/yakuake.desktop
[Desktop Entry]
Type=Application
Exec=/usr/bin/yakuake
Hidden=false
NoDisplay=false
X-GNOME-Autostart-enabled=true
Name[en_CA]=yakuake
Name=yakuake
Comment[en_CA]=
Comment=
EOF
fi

# git clone my repos
[ -e ~/bin/myrepos ] || (cd ~/src/scripts && ./setup.sh)
(cd ~/bin && ./myrepos pull)
(cd ~/src/dotfiles && ./setup.sh)
(cd ~/src/ssh && ./setup.sh)
(cd ~/src/elisp && ./setup.sh)

ln -sf ~/src/mm ~/
mkdir -p ~/gDrive
mkdir -p ~/tmp

# install goosync
which goosync >& /dev/null
if [ $? != 0 ]; then
  . ~/.profile
  if [ -z "$PYTHONUSERBASE" ]; then
    echo "PYTHONUSERBASE not set, skipping goosync install"
  else
    mkdir -p $PYTHONUSERBASE
    pip install --user goosync
  fi
fi

# install stuff needed to build ~/opt/repos?
if [ -f ~/opt/repos/setup_prep.sh ]; then
  while true; do
    read -p "run ~/opt/repos/setup_prep.sh as sudo? " yn
    case $yn in
      [Yy]*) (cd ~/opt/repos && sudo ./setup_prep.sh); break;;
      [Nn]*) break;;
      *) echo "Please answer y or n.";;
    esac
  done
fi

#
# TODO:
#
# - use dconf-editor to edit
#
#     org/gnome/metacity/theme and set to Radiance
#
#   this will fix title bar background color.
#
# - run in goosync in ~/gDrive
#
# - ./setup_all.sh in opt/repos
#
