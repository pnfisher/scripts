#!/bin/bash

usage () {

  echo $p: usage
  echo "-nb        # no batch mode (default is batch mode)"
  echo "-nt        # no terse mode (default is terse mode)"
  echo "-path dir  # add dir to the -path targets"
  echo

  exit 1

}

cleanup () {

  [ $1 != 0 ] && echo "cleanup ($1)... cleaning up"
  cd ${cdir}
  exit 0

}

p=`basename $0`
#udir=ssh://jaypeak//home/phil/unison/phil/
#udir=/n/GirlBook/Public/unison/phil/
udir=ssh://root@GirlBook//shares/Public/unison/phil/
cdir=`pwd`
batch="-batch"
terse="-terse"

targets="\
.authinfo \
.folders \
.emacs \
.ssh/config \
.unison/default.prf \
gnus \
bin \
src2 \
prj \
backup \
Downloads"

trap "cleanup $?" ABRT TERM INT EXIT INT HUP

while [ $# -gt 0 ]; do
  case $1 in
   -nb) batch=;;
   -nt) terse=;;
   -path) shift; targets="${targets} $1 ";;
   *) usage;;
  esac
  shift
done

uniargs="-copythreshold 100 -terse -auto ${batch} ${terse} "
#uniargs="${uniargs} -confirmbigdel=false"

#if [ ! -d ${udir} ]; then
#
#  echo backup directory \'${udir}\' doesn\'t exist
#  while [ 1 ]; do
#    read -p "create (yes/no): " yesno
#    if [ "${yesno}" != "yes" ] && [ "${yesno}" != "no" ]; then
#      echo "please answer yes or no"
#      continue
#    fi
#    break
#  done
#
#  if [ "${yesno}" == "no" ]; then
#    exit 0
#  fi
#
#  mkdir -p ${udir}
#  if [ $? != 0 ]; then
#    echo $p: error, couldn\'t make directory \'${udir}\'
#    exit 1
#  fi
#
#fi

cd /home/phil || exit 1

patharg=""

for t in ${targets}; do

  patharg="${patharg} -path $t "

done

set -x
unison ${uniargs} -root /home/phil/ -root ${udir} ${patharg}
set +x
if [ $? != 0 ]; then
  echo $p: error, unison returned $?
  exit 1
fi
