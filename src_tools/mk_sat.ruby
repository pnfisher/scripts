#!/bin/sh

#
# mk (build) code analysis tools for ~/opt/ruby
#

dir=~/opt/ruby

[ ! -d ${dir} ] && echo "$0: '${dir}' missing" && exit 1

(

  cd ${dir} || exit 1

  which glimpseindex > /dev/null
  [ $? != 0 ] && echo "$0: glimpseindex not installed" && exit 0
  
  files=".glimpse_filenames"
  [ ! -f ${files} ] || mv -f ${files} ${files}.old

  rm -f .glimpse_include
  cat > .glimpse_include <<EOF
1.8
EOF

  rm -f .glimpse_exclude
  cat > .glimpse_exclude <<EOF
doc/*
specifications/*
*/test/*
.so$
/.*$
/ChangeLog$
/CHANGELOG$
/LICENSE$
/LICENSE.txt$
/RELEASENOTES$
/README$
/CHANGES$
/AUTHORS$
/COPYING$
/NEWS$
/MIT-LICENSE$
/README$
/TODO$
/USAGE$
/RUNNING_UNIT_TESTS$
/src.files
/latest_source_cache
/source_cache
/cscope
/TAGS
/ID
EOF

  #
  # run glimpse index
  #
  if [ -f .glimpse_include ]; then
    echo "$0: here are the contents of .glimpse_include"
    cat .glimpse_include
  fi
  if [ -f .glimpse_exclude ]; then
    echo "$0: here are the contents of .glimpse_exclude"
    cat .glimpse_exclude
  fi
  echo glimpseindex -H `pwd` -o -w 1000 -n .
  glimpseindex -H `pwd` -o -w 1000 -n .
  [ $? != 0 ] && "$0: glimpseindex error" && exit 1

  new=`head -n 1 ${files}`
  if [ -f ${files}.old ]; then
    old=`head -n 1 ${files}.old`
    echo "glimpseindex finished: new file count ${new}, old file count ${old}"
  else
    echo "glimpseindex finished: file count ${new}"
  fi

  # make a copy of .glimpse_filenames without the file count line
  rm -f src.files
  echo "tail -n +2 ${files} > src.files"
  tail -n +2 ${files} > src.files
  files=src.files

  #
  # run cscope
  #

  which cscope > /dev/null
  if [ $? != 0 ]; then
    echo "$0: cscope not installed, skipping"
  else
    rm -f cscope.out
    echo cscope -b -k -i ${files}
    cscope -b -k -i ${files}
    [ $? != 0 ] && "$0: cscope error" && exit 1
    ls -l cscope.out
  fi

  #
  # run rtags
  #

  which rtags > /dev/null
  if [ $? != 0 ]; then
    echo "$0: rtags not installed, skipping"
  else
    rm -f TAGS
    echo rtags --quiet -R .
    rtags --quiet -R .
    [ $? != 0 ] && "$0: mkid error" && exit 1
    ls -l TAGS
  fi

)
