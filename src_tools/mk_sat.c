#!/bin/bash

#
# mk (build) code analysis tools for javascript examples
#

usage () {

  echo "$0: usage"
  echo "  [-d dir]"
  exit 1

}

dir=

while [ $# -gt 0 ]; do
  case "$1" in
    -d) shift; dir=$1;;
    *)  usage ;;
    esac
    shift
done

[ "${dir}" = "." ] && dir=`pwd`
[ "${dir}" = "" ] && echo "$0: missing argument" && usage
[ ! -d ${dir} ] && echo "$0: '${dir}' missing" && exit 1

(

  cd ${dir} || exit 1

  which glimpseindex > /dev/null
  [ $? != 0 ] && echo "$0: glimpseindex not installed" && exit 0

  files=".glimpse_filenames"
  [ ! -f ${files} ] || mv -f ${files} ${files}.old

  rm -f .glimpse_include
  cat > .glimpse_include <<EOF
\.sh$
Makefile*
makefile*
\.awk$
\.h$
\.c$
\.md$
\.txt$
\.py$
\.sh$
\.yml$
EOF

  rm -f .glimpse_exclude
  cat > .glimpse_exclude <<EOF
*
EOF

  #
  # run glimpse index
  #
  if [ -f .glimpse_include ]; then
    echo "$0: here are the contents of .glimpse_include"
    cat .glimpse_include
  fi
  if [ -f .glimpse_exclude ]; then
    echo "$0: here are the contents of .glimpse_exclude"
    cat .glimpse_exclude
  fi
  echo glimpseindex -H `pwd` -o -w 1000 -n -i .
  glimpseindex -H `pwd` -o -w 1000 -n -i .
  [ $? != 0 ] && "$0: glimpseindex error" && exit 1

  new=`head -n 1 ${files}`
  if [ -f ${files}.old ]; then
    old=`head -n 1 ${files}.old`
    echo "glimpseindex finished: new file count ${new}, old file count ${old}"
  else
    echo "glimpseindex finished: file count ${new}"
  fi

  # make a copy of .glimpse_filenames without the file count line
  rm -f src.files
  echo "tail -n +2 ${files} > src.files"
  tail -n +2 ${files} > src.files
  files=src.files

  #
  # run etags
  #

  which etags > /dev/null
  if [ $? != 0 ]; then
    echo "$0: etags not installed, skipping"
  else
    rm -f TAGS
    echo "cat ${files} | etags -o TAGS -"
    cat ${files} | etags -o TAGS -
    [ $? != 0 ] && "$0: etags error" && exit 1
    ls -lh TAGS
  fi

  #
  # run gtags
  #

  which gtags > /dev/null
  if (($? != 0)); then
    echo "$0: gtags not installed, skipping"
  else
    set -x
    #gtags -I -o -c -f ${files}
    gtags -I -o -c
    set +x
    [ $? != 0 ] && "$0: gtags error" && exit 1
    ls -lh GPATH GRTAGS GTAGS ID
  fi

)
