#!/bin/bash

sdir=${HOME}/src/scripts/src_tools

if [ ! -d ${sdir} ]; then
  echo "error, ${sdir} is missing" 1>&2
  exit 1
fi

if (($# < 1 )); then
  echo "error, mandatory postfix/lang arg missing" 1>&2
  echo "here are the available options:" 1>&2
  ls -1 ${sdir}/ | sed 's/mk_sat\./  /g' 1>&2
  exit 1
fi

mk_sat=${sdir}/mk_sat.$1
shift

if [ ! -x ${mk_sat} ]; then
  echo "error, ${mk_sat} doesn't exist" 1>&2
  echo "here are the available scripts:" 1>&2
  ls -1 ${sdir}/ 1>&2
  exit 1
fi

exec ${mk_sat} $*
