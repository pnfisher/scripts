#!/bin/bash

function usage () {

  echo $p: usage
  echo "-c             # if debug mode, don't halt boot waiting for attach"
  echo "-d             # start in debug mode"
  echo "-i file        # boot specified image file"
  echo "-h host        # boot specified host"
  echo "-ih            # boot using initramfs /init-hack script"
  echo "-k             # boot ~/qemu/linux kernel"
  echo "-ka kargs      # kernel args"
  echo "-m mem         # boot guest with size mem amount of memory"
  echo "-n             # don't use -enable-kvm"
  echo "-qc            # use my custom qemu"
  echo "-t port        # telnet port"
  echo "-v             # verbose mode"
  echo
  exit 1

}

function cleanup () {

  [ $1 != 0 ] && echo "$p: cleanup ($1)... cleaning up"
  [ "$mode" = "helper" ] || ${dscript} ${iface}
  exit $1

}

function kvmargs () {

  echo " "starting kvm with the following command line arguments:

  count=0

  for c in $args; do
    if [[ ${c} =~ ^- ]] && ! [[ ${c} =~ ^-- ]]; then
      (( count++ )) && echo
    else
      echo -n " "
    fi
    echo -n " "$c

  done

  echo

}

brhelper=/usr/lib/qemu/qemu-bridge-helper
p=`basename $0`
uscript=/home/phil/bin/qemu-ifup
dscript=/home/phil/bin/qemu-ifdown
imgfile=
host=
# default qemu provided by ubuntu
#cmd=/usr/bin/kvm
cmd=/usr/bin/qemu-system-x86_64
drive_arg=",media=disk"
iface=tap0
kernel=
verbose=
debug=
stop="-S"
args=
kvm="-enable-kvm"
mem=512
#init="init=/sbin/init"
init=
port=
kargs=
mode=

while (( $# > 0 )); do
  case $1 in
   -c) stop=;;
   -d) args="-s ";;
   -h) shift; host=$1;;
   -i) shift; imgfile=$1; drive_arg=;;
   -ih) init="init=/run/init-hack";;
   -k) kernel=1;;
   -ka) shift; kargs="${kargs}$1 ";;
   -m) shift; mem=$1;;
   -n) kvm=;;
   -qc) cmd=./kvm;;
   -t) shift; port=$1;;
   -v) verbose=1;;
   *) usage
   esac
   shift
done

#[ -n "$verbose" ] && init="$init -v"

[ -z "$port" ] && echo $p: error, no telnet port specified && exit 1
[ -z "$host" ] && echo $p: error, no target host specified && exit 1

[ -z "$imgfile" ] && imgfile=${host}/${host}.img

if [ -n "$args" ]; then
  if [ -z "$kernel" ]; then
    echo $p: you must specify -k when using -d
    exit 1
  fi
  args="$args $stop "
fi

[ -n "$kargs" ] && kernel=1

if [ -n "$kvm" ]; then
  lsmod | grep kvm >& /dev/null
  if (( $? == 0 )); then
    args="$args -enable-kvm"
  else
    echo $p: warning, kvm modules not loaded, skipping -kvm-enable
  fi
fi

if [ -x $brhelper -a -e /etc/qemu/bridge.conf ]; then
  mode=helper
fi

args="$args -machine pc"
args="$args -m ${mem}"
args="$args -smp 1,sockets=1,cores=1,threads=1"
args="$args -name sugarloaf"
args="$args -nodefconfig"
args="$args -rtc base=utc"
#args="$args -serial stdio"
args="$args -serial telnet::${port},server,wait"
args="$args -monitor telnet::$((port+1)),server,nowait"
#args="$args -monitor pty"
args="$args -nographic"

args="$args -drive file=${imgfile},if=none,id=drive-virtio-disk0${drive_arg}"
args="$args -device virtio-blk-pci,bus=pci.0,addr=0x5,drive=drive-virtio-disk0,id=virtio-disk0,bootindex=1"

#args="$args -netdev type=user,id=hostnet0,hostname=sugarloaf,net=192.168.1.0/24,dhcpstart=192.168.1.20"
if [ "$mode" = "helper" ]; then
  args="$args -netdev tap,id=hostnet0,helper=$brhelper"
else
  args="$args -netdev tap,id=hostnet0,ifname=${iface},script=no,downscript=no"
fi
#args="$args -device e1000,netdev=hostnet0,mac=52:54:00:f6:92:b2"
args="$args -device virtio-net-pci,netdev=hostnet0,mac=52:54:00:f6:92:b2"

args="$args -device virtio-balloon-pci,id=balloon0,bus=pci.0,addr=0x6"

(

cd ~/qemu >& /dev/null
[ $? != 0 ] && echo $p: ~/qemu directory missing && exit 1

if [ -n "$kernel" ]; then
  bzImage="$host/bzImage"
  if [ ! -e $bzImage ]; then
    echo $p: bzImage \'$bzImage\' missing
    exit 1
  fi
  initrd=$host/initrd
  if [ ! -e $initrd ]; then
    echo $p: initrd \'$initrd\' missing
    exit 1
  fi
  args="$args -kernel ${bzImage} -initrd ${initrd} "
  args="$args -append \
  \"root=/dev/vda1 console=ttyS0,115200n8 ${init} ${kargs}\""
fi

if [ ! -e ${imgfile} ]; then
  echo $p: couldn\'t find ${imgfile}
  exit 1
fi

if [ ! -e ${uscript} ]; then
  echo $p: couldn\'t find qemu-ifup script
  exit 1
fi

if [ ! -e ${dscript} ]; then
  echo $p: couldn\'t find qemu-ifdown script
  exit 1
fi

trap "cleanup $?" ABRT TERM INT EXIT

echo $p: $($cmd -version)

if [ "$mode" != "helper" ]; then
  ${uscript} ${iface} >& /dev/null
  if [ $? != 0 ]; then
    echo $p: couldn\'t setup ${iface}
    exit 1
  fi
fi
cmd="$cmd $args"
[ -n "$verbose" ] && kvmargs
#set -x
eval $cmd
exit $?

)

exit $?

#####
##### other args worth considering
#####

#-alt-grab \
#-vga cirrus \
#-vnc 127.0.0.1:0


#-daemonize

#if [ $? == 0 ]; then
#  gvncviewer 127.0.0.1:0 &
#fi
#wait $pid
#echo bye

#-curses \

#-serial stdio \

#-vga std \
#-vnc 127.0.0.1:0

#-nodefaults \
#-display curses \
#-vga std \
#-chardev socket,id=charmonitor,path=/home/phil/src/hacking/sugarloaf.monitor,server,nowait \
#-mon chardev=charmonitor,id=monitor,mode=control \
#-monitor stdio \
#-nographic

#-vnc 127.0.0.1:0 \


#-monitor stdio \
#-nographic


#-nodefaults \




#-vga cirrus \


#-chardev pty,id=charserial0 \
#-device isa-serial,chardev=charserial0,id=serial0 \


#-device intel-hda,id=sound0,bus=pci.0,addr=0x4 \
#-device hda-duplex,id=sound0-codec0,bus=sound0.0,cad=0 \
#-usb \



#-boot d -hda $imgfile \
#-cdrom $isofile

#-no-reboot \
#-no-shutdown \
