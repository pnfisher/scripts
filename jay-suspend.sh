#!/bin/bash -x

p=`basename $0`
host=`hostname`

if [ "$host" != "jaypeak" ]; then
  ping -c 1 jaypeak &> /dev/null
  if [ $? != 0 ]; then
    echo $p: host jaypeak not responding to ping
    exit 1
  fi
fi

guests=$(ssh jaypeak virsh list | grep running | awk '{ print $2 }')

if [ -z "$guests" ]; then
  ssh -t jaypeak sudo pm-suspend
  exit 0
fi

for g in $guests; do
  ssh jaypeak virsh suspend $g
done

for g in $guests; do
  for ((i=0;i<5;i++)); do
    ssh jaypeak virsh list | grep $g | grep paused >& /dev/null
    [ $? = 0 ] && break
    sleep 1
  done
done

ssh -t jaypeak sudo pm-suspend

for g in $guests; do
  ssh jaypeak virsh resume $g
done

for g in $guests; do
  ssh $g uname -n >& /dev/null
  if [ $? = 0 ]; then
    echo $g is up
  else
    echo $g is not up
  fi
done
