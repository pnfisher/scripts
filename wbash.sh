#!/bin/bash

wbashrc=${HOME}/scripts/.bashrc

if [ ! -f ${wbashrc} ]; then
  echo "error, ${bashrc} missing" 2>&1
  exit 1
fi

if [ "$1" = "aliases" ]; then
  export WORK_BASH=aliases
else
  export WORK_BASH=y
fi

LD_LIBRARY_PATH="${HOME}/ebw/bin/lib:${HOME}/ebw/bin/lib64:${LD_LIBRARY_PATH}"
PATH="${HOME}/ebw/bin:${HOME}/ebw/perl/bin:${PATH}"
export LD_LIBRARY_PATH PATH
[ "$1" = "theia" ] && unset LD_LIBRARY_PATH

exec bash
