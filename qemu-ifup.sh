#!/bin/sh

switch=br0

if [ -n "$1" ];then
  sudo /usr/sbin/tunctl -u phil -t $1
  sudo /sbin/ifconfig $1 0.0.0.0 up
  #/sbin/ip link set $1 up
  #sleep 0.5s
  sudo /sbin/brctl addif $switch $1
  exit 0
else
  echo "Error: no interface specified"
  exit 1
fi
