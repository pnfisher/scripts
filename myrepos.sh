#!/bin/bash

print_msg() {

  # remove extra spaces
  msg=$(echo "$1" | sed -E 's/[[:space:]]+/ /g')
  echo ${msg} && echo $(printf "%${#msg}c" "-" | sed 's/ /-/g')

}

error() {
  echo "error, $*" 1>&2
  exit 1
}

pull_repo() {

  local repo=$1
  local rebase=
  [ "$2" = "rebase" ] && rebase="--rebase"

  if [ ! -d ${repo} ]; then

    print_msg "git clone of missing repo directory (${repo})"

    dir=$(dirname ${repo})
    mkdir -p ${dir}

    (cd ${dir} && git clone git@bitbucket.org:pnfisher/$(basename ${repo}))
    (($? != 0)) && error "attempt to clone repo ${repo} failed"

  else

    print_msg "git pull ${rebase} in $(pwd)/${repo}"

    (cd ${repo} && git pull ${rebase})
    (($? != 0)) && error "git pull ${rebase} in repo ${repo} failed"

  fi

  echo

}

status_repo() {

  local repo=$1

  if [ ! -d ${repo} ]; then
    print_msg "repo ${repo} is missing"
    echo "skipping"
    echo
    return
  fi

  print_msg "git status -a in $(pwd)/${repo}"

  status=$(cd ${repo} && git status -s)
  (($? != 0)) && error "git status -a in repo ${repo} failed"

  if [ -z "${status}" ]; then
    echo "repo ${repo} is clean"
  else
    echo "${status}"
    echo "repo ${repo} is dirty"
  fi

  echo

}

repos=$(cat <<EOF
src/elisp
src/ssh
src/mm
src/etc
src/dotfiles
src/java/jdb
opt/repos
src/scripts
notes
src/python/google/goosync
EOF
)

[ -z "$1" ] && \
  error "missing command argument (valid values are [pull|status])"
[ "$1" != "pull" -a "$1" != "status" -a "$1" != "rebase" ] && \
  error "illegal command argument (valid values are [pull|status])"

cd ${HOME}

for repo in ${repos}; do

  if [ "$1" == "pull" ]; then
    pull_repo ${repo}
  elif [ "$1" == "rebase" ]; then
    pull_repo ${repo} rebase
  else
    status_repo ${repo}
  fi

done
