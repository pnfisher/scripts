#!/usr/bin/env python
"""
getimg.py

Gets the current image of the day from NASA and sets it as the
background in Gnome. The summary / description text is written
to the image.

Requires:
    PIL             (apt-get install python-imaging or pip install PIL)
    feedparser      (apt-get install python-feedparser or pip install feedparser)

Christian Stefanescu
http://0chris.com

Based on the bash script of Jessy Cowan-Sharp - http://jessykate.com
http://blog.quaternio.net/2009/04/13/nasa-image-of-the-day-as-gnome-background/

intelli_draw method from:
http://mail.python.org/pipermail/image-sig/2004-December/003064.html
"""
#import Image
#import ImageDraw
#import ImageFont
import urllib
import feedparser
import os
import commands

# Configurable settings
DOWNLOAD_FOLDER = '~/tmp/nasa/'

# Don't change stuff beyond this point
FEED_URL = 'http://www.nasa.gov/rss/lg_image_of_the_day.rss'


def get_latest_entry():
    """
    Get URL and description of the latest entry in the feed
    """
    feed = feedparser.parse(FEED_URL)
    return (feed.entries[0].enclosures[0].href, feed.entries[0].summary)


def download_file(url):
    """
    Get the latest NASA image of the day from the feed, returns the name
    of the downloaded file.
    """
    remote_file = urllib.urlopen(url)
    local_name = url.split('/')[-1]
    local_path = os.path.expanduser(os.path.join(DOWNLOAD_FOLDER, local_name))
    local_file = open(local_path, 'w')
    local_file.write(remote_file.read())
    remote_file.close()
    local_file.close()
    return local_path

if __name__ == '__main__':
    if not os.path.exists(os.path.expanduser(DOWNLOAD_FOLDER)):
        os.makedirs(os.path.expanduser(DOWNLOAD_FOLDER))
    (url, text) = get_latest_entry()
    download_file(url)


