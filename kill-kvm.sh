#!/bin/bash

pid=$(pgrep -u $(whoami) kvm)
(( $? )) || kill -9 ${pid}
exit 0

