#!/bin/bash

p=$(basename $0)

(($# < 1)) && {
  echo "$p: error, missing target host argument" 1>&2;
  echo "usage: myemacsclient host [socket]" 1>&2;
  exit 1;
}

host=$(hostname)
thost=$1
[ "$thost" = "@" ] && thost="$host"
socket=${2:-${thost}}

SETENV=". .profile; . .bashrc"
#DCMD="emacs --daemon=${socket} >& /dev/null"
DCMD="emacs --daemon=${socket}"

## use of after-make-frame-functions hook in my-xwin.el
## makes the explicit call to my-xwin-init below no longer
## necessary
# ECCMD="
# emacsclient \
# --socket-name=${socket} \
# -c \
# --eval '(my-xwin-init)' < /dev/null >& /dev/null"
ECCMD="
emacsclient \
--socket-name=${socket} \
-c < /dev/null >& /dev/null"

startup() {

  if [ "$1" != "local" ] && [ "$1" != "remote" ]; then
    echo $p: error in startup, unknown arg $1 1>&2
    exit 1
  fi

  echo "$p: starting server on ${thost} (socket = ${socket})"
  if [ "$1" = "local" ]; then
    eval ${DCMD}
  else
    ssh -n ${thost} "${SETENV}; ${DCMD}"
  fi
  [ $? != 0 ] && \
    echo "$p: ignoring server startup error (assuming server already running)"

  echo "$p: starting client on ${thost}, (socket = ${socket})"
  if [ "$1" = "local" ]; then
    eval ${ECCMD} &
  else
    ssh -n ${thost} "${SETENV}; ${ECCMD}" &
  fi

}

if [ "$thost" != "$host" ]; then
  ping -c 1 ${thost} &> /dev/null
  if [ $? != 0 ]; then
    echo $p: target host \'${thost}\' not responding to ping 1>&2
    exit 1
  fi
  startup remote
  exit
fi

startup local

echo -e "\n$p: $p is obsolete, use myemacs instead\n" 1>&2
