#!/bin/bash

host=`hostname`

exec /usr/bin/xterm -sb -sl 100 -j -name $LOGNAME@${host} -geometry 80x25+28+55 -fg gray80 -bg gray10 -fn 7x14
