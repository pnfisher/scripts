#!/bin/bash

p=$(basename $0)
repo=/home/phil/backup/repos/$(basename $(pwd))

if [ ! -d ${repo} ]; then
  echo $p: missing ${repo} directory
  exit 1
fi

set -x
[ -d .git ] && git gc
exec rsync -a --info=progress2 --delete ${repo} ../

