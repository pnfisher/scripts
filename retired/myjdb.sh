#!/bin/bash
# special wrapper for jdb which uses -a instead of -attach to specify
# an attach request. We need this to workaround an annoying feature in
# emacs gud jdb implementation which when it sees -attach in the jdb
# command line always issues a classpath command to jdb and then uses
# whatever is returned as the classpath in which to search for source
# files. This doesn't work when we are attaching to a java ee server
# and need to feed gud jdb a special classpath in order to debug a
# java ee application. End user should uses -sourcepath in conjuction
# with -a, then gud jdb will treat the specified sourcpath as the
# classpath in which to look for source files (sourcepath should be
# specified relative to the current working directory) In addition, if
# end-user has specified an -a argument (in order to trick gud jdb
# into thinking we're not doing an attach), throw out any -classpath
# arg that end-user might have specified (since it's illegal to
# specify a -classpath arg to jdb when doing an attach). User will
# have specified this -classpath arg as a way (again) to trick gud jdb
# into using it as the path in which to search for source files. If
# end user also specified a -sourcepath, then gud jdb will use the
# sourcepath relative to the classpath specification instead of the
# sourcepath relative to the current working directory when looking
# for source files.

args=
attach=
classpath=

while (($# > 0)); do
  if [[ $1 =~ -classpath.* ]]; then
    classpath=$1
  elif [[ "$1" == "-a" ]]; then
    shift
    attach=$1
  else
    args="${args} $1"
  fi
  shift
done

if [ -n "$attach" ]; then
  attach="-attach ${attach}"
  [ -n "$classpath" ] && classpath=
fi

jdb ${attach} ${classpath} ${args}
