#!/bin/sh

#
# mk (build) code analysis tools for mcp
#

usage () {

  echo "$0: usage"
  echo "  [-v view]"
  exit 1

}

view=

while [ $# -gt 0 ]; do
  case "$1" in
    -v) shift; view=$1;;
    *)  usage ;;
    esac
    shift
done

[ "${view}" = "" ] && echo "$0: missing argument" && usage

dir=/view/${view}/vobs/os_components

[ ! -d ${dir}/rose ] && echo "$0: '${dir}' not mounted?" && exit 1

(

  cd ${dir} || exit 1

  which glimpseindex > /dev/null
  [ $? != 0 ] && echo "$0: glimpseindex not installed" && exit 0
  
  files=".glimpse_filenames"
  [ ! -f ${files} ] || mv -f ${files} ${files}.old

  rm -f .glimpse_include
  cat > .glimpse_include <<EOF
EOF

  rm -f .glimpse_exclude
  cat > .glimpse_exclude <<EOF
/linux-x86/*.d
/linux-MPC*/*.d
archived/*
doc/*
lost+found/*
OIS/*
service_processors/*
SSHield/*
tatl/*
L0FabricManager/tests/*
mbs/linux/kernels/*
mbs/linux/packages/*
apps/vxworks-6.3/*
BSP/*.vsh$
BSP/*.ref$
WRWB/*.wpj$
.patch$
.keep*
.contrib*
.merge*
/.*$
Doxyfile
/mcp.files
/cscope
/TAGS
/ID
EOF

  #
  # run glimpse index
  #
  if [ -f .glimpse_include ]; then
    echo "$0: here are the contents of .glimpse_include"
    cat .glimpse_include
  fi
  if [ -f .glimpse_exclude ]; then
    echo "$0: here are the contents of .glimpse_exclude"
    cat .glimpse_exclude
  fi
  echo glimpseindex -H `pwd` -o -w 1000 -n .
  glimpseindex -H `pwd` -o -w 1000 -n .
  [ $? != 0 ] && "$0: glimpseindex error" && exit 1

  new=`head -n 1 ${files}`
  if [ -f ${files}.old ]; then
    old=`head -n 1 ${files}.old`
    echo "glimpseindex finished: new file count ${new}, old file count ${old}"
  else
    echo "glimpseindex finished: file count ${new}"
  fi

  # make a copy of .glimpse_filenames without the file count line
  rm -f mcp.files
  echo "tail -n +2 ${files} > mcp.files"
  tail -n +2 ${files} > mcp.files
  files=mcp.files

  #
  # run cscope
  #

  which cscope > /dev/null
  if [ $? != 0 ]; then
    echo "$0: cscope not installed, skipping"
  else
    rm -f cscope.out
    echo cscope -b -k -i ${files}
    cscope -b -k -i ${files}
    [ $? != 0 ] && "$0: cscope error" && exit 1
    ls -l cscope.out
  fi

  #
  # run etags
  #

  which etags > /dev/null
  if [ $? != 0 ]; then
    echo "$0: etags not installed, skipping"
  else

    rm -f TAGS

    # etags without strange (not understood) makefiles
    echo "cat ${files} | egrep -v '*\.(mk|mak)$' | etags -o TAGS -"
    cat ${files} | egrep -v '*\.(mk|mak)$' | etags -o TAGS -
    [ $? != 0 ] && "$0: etags error" && exit 1
    ls -l TAGS

    # etags with strange (not understood) makefiles
    echo "cat ${files} | egrep '*\.(mk|mak)$' | etags -l makefile -a -o TAGS -"
    cat ${files} | egrep '*\.(mk|mak)$' | etags -l makefile -a -o TAGS -
    [ $? != 0 ] && "$0: etags error" && exit 1
    ls -l TAGS

  fi

  #
  # run mkid
  #

  which mkid > /dev/null
  if [ $? != 0 ]; then
    echo "$0: mkid not installed, skipping"
  else
    rm -f ID
    echo "mkid \`cat ${files}\`"
    mkid `cat ${files}`
    [ $? != 0 ] && "$0: mkid error" && exit 1
    ls -l ID
  fi

)
