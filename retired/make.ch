#!/bin/bash

usage () {

  echo $p: usage
  echo "  -h    # help"
  echo "  -svn  # use svn repo"
  echo "  -git  # use git repo"
  echo "  *     # passed as Makefile arguments"

  exit 0

}

p=`basename $0`
repo=git

while [ $# -gt 0 ]; do
  case $1 in
   -git) repo=git;;
   -svn) repo=svn;;
   -h) usage;;
   -*) usage;;
   *) break;;
  esac
  shift
done

[ -z ${repo} ] && echo $p: error, please specify -git or -svn && exit 1

T1=`date`
T1=`date --utc "+%s"`

(cd ~/src/chromium; make -f Makefile.${repo} $*)

T2=`date --utc "+%s"`

diff=$((T2-T1))
H=$((diff/3600))
diff=$((diff%3600))
M=$((diff/60))
S=$((diff%60))

echo    $p: build started `date --date @$T1 "+%T"`
echo    $p: build stopped `date --date @$T2 "+%T"`
printf "$p: build time    %02d:%02d:%02d" $H $M $S
echo

