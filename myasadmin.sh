#!/bin/bash

JDK=${JDK:-8}

if [ "$JDK" != "7" -a "$JDK" != "8" ]; then
  echo "error, invalid JDK setting (${JDK})" 1>&2
  exit 1
fi

echo "$(basename $0 .sh): using jdk ${JDK}"

if [ "$JDK" == "8" ]; then
  export JAVA_HOME=${HOME}/java/jdk1.8.0_60
  export PATH=${JAVA_HOME}/bin:${PATH}
fi

asadmin ${arg} $*
exit $?
