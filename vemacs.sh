#!/bin/bash

print_envs () {
  echo "here are your available virtualenvs:"
  venvs=`workon`
  for v in ${venvs}; do
    echo "  $v"
  done
}

if [ ! -f /home/phil/opt/python/bin/virtualenvwrapper.sh ]; then
  echo "vemacs: no virtualenvwrapper.sh"
  exit 1
fi

. /home/phil/opt/python/bin/virtualenvwrapper.sh

if [ $# -ne 1 ]; then
  echo "vemacs: missing virtualenvwrapper workon arg"
  print_envs
  exit 1
fi

workon $1 >& /dev/null
if [ $? != 0 ]; then
  echo "vemacs: sorry, no such virtualenv as $1"
  print_envs
  exit 1
fi

emacs -T $1@`hostname` >& /dev/null &
