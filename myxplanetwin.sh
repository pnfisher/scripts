#!/bin/bash

# Add this script to System->Preferences->Sessions->Startup Programs (GNOME 2)
# or Startup Applications (GNOME 3)

function cleanup() {
  echo cleanup called
  rm -f ${pidFile}
  exit $1
}

# how long to sleep between drawings, in seconds
sleep=301

# screen size
#geometry=1680x1050
#geometry=1600x900
geometry=1360x768

# where to keep the desktop image
prefix=${HOME}/tmp/xplanet.$(hostname).${geometry}
if [ ! -d $prefix ]; then mkdir -p $prefix; fi
logFile=${prefix}/xplanet.log
pidFile=${prefix}/xplanet.pid

# location of the xplanet binary
xplanetBin=/usr/bin/xplanet

# your desired command line arguments
#xplanetArgs="-body major -random -range 20 -label -labelpos -10+30"
xplanetArgs="-radius 38 -origin sun -target earth -projection orthographic -starfreq 0.003 -wait 1800 -latitude 20 -longitude -79 -quality 100"
pid=

###
### You don't need to change anything below here
###

trap "cleanup $?" ABRT TERM INT EXIT

if [ -f ${pidFile} ]; then
  pid=$(cat ${pidFile})
  kill -9 $pid >& /dev/null
  rm -f ${pidFile}
fi
echo $$ > ${pidFile}

rm -f ${logFile}
exec >${logFile} 2>&1
[ -z "$pid" ] || echo killed pid $pid

if [ ! -x ${xplanetBin} ]; then
  echo "error, ${xplanetBin} missing"
  exit
fi

nextTime=$(date +%s)
index=0
bgFile=
prevFile=

rm -f ${prefix}/*.jpg

while true; do

  thisTime=$(date +%s)

  if [ $thisTime -lt $nextTime ]; then
    sleep 10
    continue;
  fi

  prevFile=${bgFile}
  bgFile=${prefix}/xplanet${index}.jpg
  [ -f ${bgFile} ] && rm -f ${bgFile}

  index=$((index+1))
  if ((index == 0x10000000)); then
    index=0
  fi

  echo "generating ${bgFile}"

  # The xplanet command
  ${xplanetBin} ${xplanetArgs} \
                -num_times 1 \
                -output $bgFile \
                -geometry $geometry
  if (($? != 0)); then
    echo "error, xplanet failed"
    exit
  fi

  [ ! -z "${prevFile}" ] && [ -f ${prevFile} ] && rm -f ${prevFile}

  nextTime=$((nextTime+sleep))
  echo "sleeping for ${sleep} seconds @$(date)"
  sleep $sleep
  echo "Awake @$(date)"

done
