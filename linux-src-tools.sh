#!/bin/bash

usage () {

  echo $p: usage
  echo "-d dir   # run in source directory dir"
  echo

  exit 1

}

p=$(basename $0)
glimpse=/home/phil/opt/bin/glimpseindex
srcdir=

while (( $# > 0 )); do
  case $1 in
   -d) shift; srcdir=$1;; 
    *) usage
  esac
  shift
done

[ -z "$srcdir" ] && echo $p: you must specify a source directory && exit 1
[ "$srcdir" = "." ] && srcdir=$(pwd)
if [ ! -d $srcdir ]; then
  echo $p: directory \'$srcdir\' doesn\'t exist
  exit 1
fi

[ ! -f $glimpse ] && echo $p: $glimpse not installed && exit 0

files=".glimpse_filenames"
[ ! -f $files ] || mv -f $files $files.old

rm -f .glimpse_include
cat > .glimpse_include <<EOF
.h$
.c$
.s$
.S$
.l$
.y$
.cc$
.pl$
.py$
.sh$
.txt$
.awk$
.lds$
Documentation
defconfig
TODO
HOWTO
Makefile
Kconfig
README
EOF

## not doing
#  .builtin$
#  .cmd$
#  .fuc$
#  .asp$
#  .scr$

rm -f .glimpse_exclude
cat > .glimpse_exclude <<EOF
*
EOF

(

cd $srcdir
dirs=$(find . -maxdepth 1 -type d | egrep -v "drivers|arch|^(\./\.)|\.$")
dirs="$dirs ./arch/x86_64 ./arch/x86"
dirs="$dirs $(find ./drivers -maxdepth 1 -type d | \
egrep -v "\./drivers($|/staging$)")"
dirs=$(echo $dirs | sort)

if [ -f .glimpse_include ]; then
  echo $p: here are the contents of .glimpse_include
  cat .glimpse_include
  echo
fi
if [ -f .glimpse_exclude ]; then
  echo $p: here are the contents of .glimpse_exclude
  cat .glimpse_exclude
  echo
fi

# empty for tiny, -o for small, and -b for medium
#gisize=-b
#gisize=-o
gisize=

echo $glimpse -t $gisize -i -H $(pwd) -w 1000 -n $dirs
$glimpse -t $gisize -i -H $(pwd) -w 1000 -n $dirs
[ $? != 0 ] && echo $p: $glimpse failure && exit 1

new=$(head -n 1 $files)
if [ -f $files.old ]; then
  old=$(head -n 1 $files.old)
  echo -n "$(basename $glimpse) finished: new file count $new, "
  echo "old file count $old"
else
  echo "$(basename $glimpse) finished: file count $new"
fi
ls -lh .glimpse_index

# make a copy of .glimpse_filenames without the file count line
rm -f linux.files
#echo "tail -n +2 $files > linux.files"
tail -n +2 $files > linux.files
files=linux.files
ls -lh $files

#
# run cscope
#

which cscope > /dev/null
if [ $? != 0 ]; then
  echo $p: cscope not installed, skipping
else
  rm -f cscope.out
  echo cscope -b -k -i ${files}
  cscope -b -k -i ${files} >& /dev/null
  [ $? != 0 ] && echo $p: cscope failure && exit 1
  ls -lh cscope.out
fi

#
# run etags
#

which etags > /dev/null
if [ $? != 0 ]; then
  echo $p: etags not installed, skipping
else

  rm -f TAGS

  # etags without strange (not understood) makefiles
  echo "cat ${files} | etags -o TAGS -"
  cat ${files} | etags -o TAGS -
  [ $? != 0 ] && echo $p: etags error && exit 1
  ls -lh TAGS

fi

)