#!/bin/bash

# Add this script to System->Preferences->Sessions->Startup Programs (GNOME 2)
# or Startup Applications (GNOME 3)

# how long to sleep between drawings, in seconds
sleep=300

# screen size
#geometry=1680x1050
#geometry=1600x900
geometry=1360x768

# where to keep the desktop image
prefix=${HOME}/tmp/xplanet.$(hostname).${geometry}
if [ ! -d $prefix ]; then mkdir -p $prefix; fi
logFile=${prefix}/xplanet.log

# location of the xplanet binary
xplanetBin=/usr/bin/xplanet

# your desired command line arguments
#xplanetArgs="-body major -random -range 20 -label -labelpos -10+30"
xplanetArgs="-radius 38 -origin sun -target earth -projection orthographic -starfreq 0.003 -wait 1800 -latitude 20 -longitude -79 -quality 100"

###
### You don't need to change anything below here
###

rm -f ${logFile}
exec >${logFile} 2>&1

if [ ! -x ${xplanetBin} ]; then
  echo "error, ${xplanetBin} missing"
  exit
fi

# is it already running
for xp in "myxplanet" "myxplanet.sh"; do
  for pid in $(pgrep -u ${USER} ${xp}); do
    if [ -n "$pid" -a "$pid" != "$$" ]; then
      echo "killing already running version of ${xp} (pid=${pid})"
      kill ${pid}
    fi
  done
done

# GNOME 2 and 3 set the background differently
GNOME_VERSION=$(gnome-session --version | cut -d' ' -f2 | cut -d. -f1)
if [ $? != 0 -o -z "$GNOME_VERSION" ]; then
  GNOME_VERSION=
  if [ -f /usr/share/gnome/gnome-version.xml ]; then
    GNOME_VERSION=$(grep platform /usr/share/gnome/gnome-version.xml | \
                       sed 's/.*\([0-9]\).*/\1/g')
  fi
fi

if [ -z "$GNOME_VERSION" ]; then
  echo "error, can't figure out gnome version"
  exit
else
  echo gnome version is ${GNOME_VERSION}
fi

ftype=jpeg
bgFile=${prefix}/xplanet.${ftype}

nextTime=$(date +%s)

while true; do

  thisTime=$(date +%s)

  if [ $thisTime -lt $nextTime ]; then
    sleep 10
    continue;
  fi

  # check to see if we're still logged in
  loggedIn=$(ps U $USER | grep gnome-session|grep -v grep)
  if [ -z "$loggedIn" ]; then
	  echo "exiting at $(date)"
	  exit
  fi

  # The xplanet command
  ${xplanetBin} ${xplanetArgs} \
                -num_times 1 \
                -output $bgFile \
                -geometry $geometry
  if (($? != 0)); then
    echo "error, xplanet failed"
    exit
  fi

  if [ $GNOME_VERSION -eq 2 ]; then
    # GNOME 2
	  gconftool -t str -s /desktop/gnome/background/picture_filename $bgFile
  else
    # GNOME 3
	  gsettings set org.gnome.desktop.background picture-uri file://${bgFile}
  fi

  nextTime=$((nextTime+sleep))
  sleep $sleep

done
