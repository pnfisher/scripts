#!/bin/bash

check_rc() {
  if (($1 != 0)); then
    shift
    echo "error, $*"
    exit 1
  fi
}

if (($# > 1)) || [ $# == 1 -a "$1" != "kill" \
                      -a "$1" != "destroy" \
                      -a "$1" != "firefox" \
                      -a "$1" != "restart" \
                      -a "$1" != "ssh" ]; then
  echo "error," 1>&2
  echo "  [firefox|kill|destroy|restart|ssh]" 1>&2
  echo "  are the only optional arguments supported" 1>&2
  exit 1
fi

browser="chrome"
lxc=mychrome$(hostname)

if [ "$1" == "destroy" ]; then
  ulxc destroy ${lxc}
  exit $?
fi

lxc-info -n ${lxc} >& /dev/null
if [ $? != 0 ]; then
  [ "$1" == "kill" ] && exit 0
  echo "warning, ulxc ${lxc} doesn't exist, attempting to create" 1>&2
  lxc-info -n chrome >& /dev/null
  if [ $? != 0 ]; then
    echo "error, ulxc chrome doesn't exist, can't clone" 1>&2
    exit 1
  fi
  ulxc clone chrome ${lxc}
fi

if [ "$1" == "restart" ]; then
  echo "stopping lxc ${lxc}"
  lxc-stop -n ${lxc} >& /dev/null
  echo "waiting for lxc ${lxc} to start"
  lxc-start -n ${lxc} -d >& /dev/null
  #check_rc $? "couldn't start lxc ${lxc}"
  lxc-wait -n ${lxc} -s RUNNING -t 10
  check_rc $? "couldn't start lxc ${lxc}"
fi

for((i=0;i<30;i++)); do
  ping -c 1 -W 1 ${lxc} >& /dev/null
  case $? in
    0) break;;
    *) sleep 1;;
  esac
  echo -n .
done

echo

if((i==30)); then
  echo "sorry, ${lxc} not responding to ping"
  exit 1
fi

if [ "$1" == "firefox" ]; then
  browser="firefox"
fi

if [ "$1" == "kill" ]; then
  for((i=0;i<5;i++)); do
    ssh ${lxc} pkill chrome >& /dev/null
    sleep 1
    ssh ${lxc} pgrep chrome >& /dev/null
    [ $? != 0 ] && break
  done
fi

if [ "$1" == "ssh" ]; then
  exec ssh ${lxc}
  echo "error, this line shouldn't ever execute" 1>&2
  exit 1
fi

ssh ${lxc} pgrep ${browser} >& /dev/null
if (($? == 0)); then
  echo "sorry, ${browser} already seems to be running"
  exit 1
fi

gargs[0]="--start-maximized"
gargs[1]="--disable-ssl-false-start"

if [ "${browser}" == "chrome" ]; then
  ssh -Y ${lxc} "DISPLAY=:0.0 /usr/bin/google-chrome ${gargs[*]} >& /dev/null" &
else
  ssh -Y ${lxc} "DISPLAY=:0.0 /usr/bin/firefox >& /dev/null" &
fi
exit 0

##
## ignore
##

# --disable-ssl-false-start seems to fix/improve the problem I
# was attempting to solve with all the crud that follows below
numtries=20

for((i=0;i<numtries;i++)); do

  ssh ${lxc} pgrep chrome >& /dev/null
  if (($? == 0)); then
    echo "sorry, chrome already seems to be running"
    exit 1
    #ssh ${lxc} pkill chrome
    #sleep 1
  fi
  ssh ${lxc} "env DISPLAY=:0.0 google-chrome >& /dev/null" &
  echo "waiting 1 second for chrome to start ($((i+1))/$numtries)"
  sleep 1
  count=$(ssh ${lxc} pgrep -f "chrome.*type=renderer" | wc -l)
  # if we run without extensions, then 10 is going to be a meaningless
  # test which we will always fail; its use depends upon us having at
  # least 10 extensions installed and expected to load
  if (($? != 0 || count < 10)); then
    ssh ${lxc} pkill chrome
    echo "sorry, chrome seems to have hung (render count = $count)"
    echo -n "going to kill it and try again: "
    for((j=0;j<10;j++)); do
      ssh ${lxc} pgrep chrome >& /dev/null
      if (($? == 0)); then
        echo -n .
        #sleep 1
      else
        echo
        break
      fi
    done
    continue
  else
    echo "success, chrome seems to be up"
  fi

  break

done

if ((i == numtries)); then
  ssh ${lxc} pkill chrome
  echo "sorry, couldn't get chrome up and running on ${lxc}"
  exit 1
fi

#lxc-attach --clear-env -n brome -- sudo -u phil env DISPLAY=:0.0 google-chrome >& /dev/null &
