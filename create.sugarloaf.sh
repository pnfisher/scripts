#!/bin/bash

p=`basename $0`
uscript=/home/phil/bin/qemu-ifup
dscript=/home/phil/bin/qemu-ifdown

function cleanup () {

  [ $1 != 0 ] && echo "$p: cleanup ($1)... cleaning up"
  ${dscript} ${iface} >& /dev/null
  exit $1

}

isofile=/home/phil/Downloads/philbook/ubuntu/ubuntu-12.04.3-server-amd64.iso
imgfile=/home/phil/qemu/sugarloaf.img
iface=tap0

args=
args="$args -M pc-1.0"
args="$args -enable-kvm"
args="$args -m 512"
args="$args -smp 1,sockets=1,cores=1,threads=1"
args="$args -name sugarloaf"
args="$args -nodefconfig"
args="$args -nodefaults"
args="$args -rtc base=utc"
#args="$args -no-reboot"
#args="$args -no-shutdown"

args="$args -drive file=${imgfile},if=none,id=drive-virtio-disk0,format=raw"
args="$args -device virtio-blk-pci,bus=pci.0,addr=0x5,drive=drive-virtio-disk0,id=virtio-disk0,bootindex=2"

args="$args -drive file=${isofile},if=none,media=cdrom,id=drive-ide0-1-0,readonly=on,format=raw"
args="$args -device ide-drive,bus=ide.1,unit=0,drive=drive-ide0-1-0,id=ide0-1-0,bootindex=1"

args="$args -netdev tap,id=hostnet0,ifname=${iface},script=no,downscript=no"
args="$args -device e1000,netdev=hostnet0,mac=52:54:00:f6:92:b2"

args="$args -device virtio-balloon-pci,id=balloon0,bus=pci.0,addr=0x6"
args="$args -vga cirrus"
args="$args -vnc 127.0.0.1:0"
args="$args -daemonize"

if [ ! -e ${uscript} ]; then
  echo $p: couldn\'t find qemu-ifup script
  exit 1
fi

if [ ! -e ${dscript} ]; then
  echo $p: couldn\'t find qemu-ifdown script
  exit 1
fi

if [ ! -f ${imgfile} ]; then
  qemu-img create -f raw ${imgfile} 8G
  if [ $? != 0 ]; then
    echo $p: qemu-img failed
    exit 1   
  fi
fi

trap "cleanup $?" ABRT TERM INT EXIT

#${uscript} ${iface} >& /dev/null
${uscript} ${iface}
if [ $? != 0 ]; then
  echo $p: couldn\'t setup ${iface}
  exit 1
fi

set -x
pid=`/usr/bin/kvm ${args}`
rc=$?
set +x

if [ $rc == 0 ]; then
  gvncviewer 127.0.0.1:0 &
  wait $pid
  echo bye
else
  echo sorry
  exit $rc
fi



