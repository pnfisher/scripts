#!/bin/bash

pname=$(basename $0)

if [ $(whoami) != "root" ]; then
  echo "error, please run using sudo" 1>&2
  exit 1
fi

. /etc/lsb-release || {
  echo "error, couldn't source /etc/lsb-release" 1>&2;
  exit 1;
}

if [ "$DISTRIB_CODENAME" != "xenial" ]; then
  echo "error, current distro \'${DISTRIB_CODENAME}\' not xenial" 1>&2
  exit 1
fi

## skipping for now
# xinetd tftpd tftp
packages="\
xplanet \
gnome-session-flashback \
gnome-tweak-tool \
compizconfig-settings-manager \
compiz-plugins \
ssh \
emacs \
git gitk \
yakuake \
lxc \
dconf-editor \
texinfo \
htop \
gksu \
python-pip \
python3-pip \
ack-grep \
whois \
ansible \
samba \
smbclient \
cifs-utils \
openvpn \
minicom \
screen \
sshpass \
"
builddeps=

while true; do
  read -p "install open-vm-tools? " yn
  case $yn in
    [Yy]*) packages+=" open-vm-tools open-vm-tools-desktop "; break;;
    [Nn]*) break;;
    *) echo "Please answer y or n.";;
  esac
done

apt-get update
apt-get -y upgrade

for p in ${packages}; do
  echo ${pname}: doing install $p
  apt-get -y -q install $p
done

for p in ${builddeps}; do
  echo ${pname}: doing build dep $p
  apt-get -y -q build-dep $p
done

service ssh status >& /dev/null
if [ $? != 0 ]; then
  service ssh start
  systemctl enable ssh
fi

cat /etc/apt/sources.list | grep -e "^deb-src" >& /dev/null
if [ $? != 0 ]; then
  cat /etc/apt/sources.list | sed 's/^deb/deb-src/g' >> /etc/apt/sources.list
fi

which gcc-6 >& /dev/null
if [ $? != 0 ]; then
  add-apt-repository ppa:ubuntu-toolchain-r/test -y
  apt-get update
  apt-get -y install gcc-6 g++-6 gcc-6-doc gcc-6-multilib
fi

# google chrome
which google-chrome >& /dev/null
if [ $? != 0 ]; then
  wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | \
    sudo apt-key add -
  sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list'
  apt-get update
  apt-get -y install google-chrome-stable
  rm -f /etc/apt/sources.list.d/google.list
fi

##
## run ./setup_user.sh instead
##
# sudo -l -u phil bash -v <<EOF
# mkdir -p ~/gDrive
# mkdir -p ~/tmp
# ln -sf ~/src/mm ~/mm
# if [ ! -f ~/.config/autostart/myxplanet.desktop ]; then
# mkdir -p ~/.config/autostart
# cat <<EOF2 > ~/.config/autostart/myxplanet.desktop
# [Desktop Entry]
# Type=Application
# Exec=/home/phil/bin/myxplanet
# Hidden=false
# NoDisplay=false
# X-GNOME-Autostart-enabled=true
# Name[en_CA]=xplanet
# Name=xplanet
# Comment[en_CA]=
# Comment=
# EOF2
# if [ ! -f ~/.config/autostart/yakuake.desktop ]; then
# mkdir -p ~/.config/autostart
# cat <<EOF2 > ~/.config/autostart/yakuake.desktop
# [Desktop Entry]
# Type=Application
# Exec=/usr/bin/yakuake
# Hidden=false
# NoDisplay=false
# X-GNOME-Autostart-enabled=true
# Name[en_CA]=yakuake
# Name=yakuake
# Comment[en_CA]=
# Comment=
# EOF2
# [ -e ~/bin/myrepos ] || (cd ~/src/scripts && ./setup.sh)
# (cd ~/bin && ./myrepos pull)
# (cd ~/src/dotfiles && ./setup.sh)
# (cd ~/src/ssh && ./setup.sh)
# (cd ~/src/elisp && ./setup.sh)
# (cd ~/src/mm && ./setup.sh)
# EOF
