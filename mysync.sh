#!/bin/bash

function usage () {

  echo $p: usage
  echo "-d             # use --delete"
  echo "-f             # fetch from remote"
  echo "-force         # override timestamp safety check"
  echo "-p             # pull from remote"
  echo "-x             # don't use --dry-run"
  echo
  exit 1

}

local="${HOME}"
rhost="root@GirlBook"
rdir="/shares/Public/backup/phil.rsync"
remote="$rhost:$rdir"

hostname=`hostname`
p=`basename $0`
force=
delete=
fetch=
push=
dryrun="--dry-run"
ffrom="${HOME}/src/scripts/mysync.include"
fexclude="${HOME}/src/scripts/mysync.exclude"
stamp=".mysync.stamp"

while (( $# > 0 )); do
  case $1 in
    -d) delete="--delete";;
    -f) fetch=yes;;
    -force) force=yes;;
    -p) push=yes;;
    -x) dryrun=;;
    *) usage
  esac
  shift
done

if [ ! -f $ffrom ]; then
  echo $p: error, $ffrom missing
  exit 1
fi

if [ ! -f $fexclude ]; then
  echo $p: error, $fexclude missing
  exit 1
fi

rsync_args="$dryrun $delete --update -i --modify-window=1"
rsync_args="$rsync_args --files-from=$ffrom"
rsync_args="$rsync_args --exclude-from=$fexclude"
if [ $hostname != "jaypeak" ]; then
  rsync_args="$rsync_args --exclude='+backup/qemu'"
fi
# must be last because of -r and use of exclude-from
rsync_args="$rsync_args -xarhPS"

if [ -z "$push" ] && [ -z "$fetch" ]; then
  echo "$p: error, must specify one of -f (fetch) or -p (push)"
  exit 1
fi

if [ ! -z "$push" ] && [ ! -z "$fetch" ]; then
    echo "$p: error, must specify only one of -f (fetch) or -p (push)"
    exit 1
fi

if [ ! -z "$push" ]; then

  src=$local
  dst=$remote

  if [ -z "$force" ]; then

    srctime=`stat -c %Y $src/$stamp`
    if [ $? != 0 ]; then
      echo "$p: warning"
      echo
      echo "    $src/$stamp missing"
      echo
      echo "  Can't carry out proper timestamp safety check."
      echo "  Maybe you should do a fetch first? (use -force to override)"
      echo
      exit 1
    fi

    dsttime=`ssh $rhost stat -c %Y $rdir/$stamp`
    if [ $? != 0 ]; then
      echo "$p: warning"
      echo
      echo "    $dst/$stamp missing"
      echo
      echo "  Can't carry out proper timestamp safety check."
      echo "  Maybe you should do a fetch first? (use -force to override)"
      echo
      exit 1
    fi

    if [ $dsttime -gt $srctime ]; then
      echo "$p: warning"
      echo
      echo "    $dst/$stamp"
      echo
      echo "  is newer than"
      echo
      echo "    $src/$stamp"
      echo
      echo "  Maybe you should do a fetch first? (use -force to override)"
      echo
      exit 1
    fi
  fi
else
  src=$remote
  dst=$local
fi

echo "$p: executing"
echo "  rsync $rsync_args $src $dst"
echo
rsync $rsync_args $src $dst
if [ $? -ne 0 ]; then
  echo $p: rsync error, quitting
  exit 1
fi

if [ ! -z "$push" ] && [ -z "$dryrun" ]; then
  touch $src/$stamp
  rsync -av $src/$stamp $dst/
fi
