#!/bin/sh

export PATH="/opt/bin:${PATH}"

#
# mk (build) code analysis tools for any old directory
#

usage () {

  echo "$0: usage"
  echo "  [-d dir]"
  exit 1

}

p=$(basename $0)
glimpse=/home/phil/opt/bin/glimpseindex
dir=

while [ $# -gt 0 ]; do
  case "$1" in
    -d) shift; dir=$1;;
    *)  usage ;;
    esac
    shift
done

[ "${dir}" = "." ] && dir=`pwd`
[ "${dir}" = "" ] && echo "$p: missing argument" && usage
[ ! -x $glimpse ] && echo $p: $glimpse not installed && exit 0
[ ! -d ${dir} ] && echo "$p: '${dir}' missing" && exit 1

(

  cd ${dir} || exit 1
  
  files=".glimpse_filenames"
  [ ! -f ${files} ] || mv -f ${files} ${files}.old

  rm -f .glimpse_include
  cat > .glimpse_include <<EOF
EOF

  rm -f .glimpse_exclude
  cat > .glimpse_exclude <<EOF
/TAGS$
/cscope.out$
/default.files$
/.glimpse*$
EOF

  #
  # run glimpse index
  #
  if [ -f .glimpse_include ]; then
    echo "$p: here are the contents of .glimpse_include"
    cat .glimpse_include
  fi
  if [ -f .glimpse_exclude ]; then
    echo "$p: here are the contents of .glimpse_exclude"
    cat .glimpse_exclude
  fi

  echo $glimpse -H `pwd` -w 1000 -n .
  $glimpse -H `pwd` -w 1000 -n .
  [ $? != 0 ] && "$p: $glimpse error" && exit 1

  new=`head -n 1 ${files}`
  if [ -f ${files}.old ]; then
    old=`head -n 1 ${files}.old`
    echo -n "$(basename $glimpse): finished, new file count ${new}, "
    echo "old file count ${old}"
  else
    echo "$(basename $glimpse): finished, file count ${new}"
  fi
  ls -lh .glimpse_index

  # make a copy of .glimpse_filenames without the file count line
  rm -f default.files
  #echo "tail -n +2 ${files} > default.files"
  tail -n +2 ${files} > default.files
  files=default.files

  #
  # run cscope
  #

  which cscope > /dev/null
  if [ $? != 0 ]; then
    echo "$p: cscope not installed, skipping"
  else
    rm -f cscope.out
    echo cscope -b -k -i ${files}
    cscope -b -k -i ${files}
    [ $? != 0 ] && "$p: cscope error" && exit 1
    ls -lh cscope.out
  fi

  which etags > /dev/null
  if [ $? != 0 ]; then
    echo "$p: etags not installed, skipping"
  else
    rm -f TAGS
    echo "cat ${files} | etags -o TAGS -"
    cat ${files} | etags -o TAGS -
    [ $? != 0 ] && "$p: etags error" && exit 1
    ls -lh TAGS
  fi

)
