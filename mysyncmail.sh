#!/bin/bash

if [ ! -d ~/rmail ]; then
  echo "error, ~/rmail directory is missing" 1>&2
  exit 1
fi

thost=$1

if [ -z "$thost" ]; then
  echo "error, target host argument is missing" 1>&2
  exit 1
fi

(
  cd ~/
  rsync -av --info=progress ~/rmail/ ${thost}:~/rmail/
  if [ $? != 0 ]; then
    echo "sorry, rsync failure" 1>&2
    exit 1
  fi
)
exit $?
